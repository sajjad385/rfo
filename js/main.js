$(document).ready(function() {
    $('.details-media').venobox();
});

$(".dropdown-toggle").on("hover",function(){
    $(this).siblings(".dropdown-menu").toggleClass("show")
})
/*sticky navbar js here*/
var mn = $(".navbar");
var mns = "header-fixed";
var hdr = $('.header-top').height();
var mnh = $(".navbar").height();
$(window).on("scroll", function() {
    if ($(this).scrollTop() > hdr) {
        mn.addClass(mns);
        $('.header-top').css("margin-bottom", mnh);
        mn.css("transition"," all 0.7s ease 0s")

    } else {
        mn.removeClass(mns);
        $('.header-top').css("margin-bottom", 0);
        mn.css("transition"," all 0.5s ease 0s")
    }
});
var iScrollPos = 0;
$(window).on("scroll",function () {
    var iCurScrollPos = $(this).scrollTop();
    if (iCurScrollPos > iScrollPos) {
        mn.addClass("scrollDown")
    } else {
        mn.removeClass("scrollDown")
    }
    iScrollPos = iCurScrollPos;
});


var drh = $("#details-top").height() + $("header").height();
$(window).on("scroll", function() {
    if ($(this).scrollTop() > drh) {
        $(".d-right .fixed-content").addClass("d-right-fixed");
    } else if($(this).scrollTop() < $("#details-top").height()){
        $(".d-right .fixed-content").removeClass("d-right-fixed");
    }
});